import { Component } from '@angular/core';

@Component({
  selector: 'ftr-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'fitrack';
}
